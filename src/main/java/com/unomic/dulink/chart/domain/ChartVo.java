
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	//common
	Integer shopId;
	String appId;
	String appName;
	String fileLocation;
	String categoryId;
	Integer dvcId;
	String id;
	String fileName;
	String ty;
	String pwd;
	String url;
	String name;
	String msg;
	String rgb;
}
