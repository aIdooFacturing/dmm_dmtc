package com.unomic.dulink.chart.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class DeviceVo{
	String adtId;
	String dvcId;
	String adapterId;
	String lastUpdateTime;
	String lastStartDateTime;
	String lastChartStatus;
	Integer lastFeedOverride;
	Float lastSpdLoad;
	Integer shopId;
	String lastAlarm;
	String lastModal;
	String workDate;
	String lastProgramName;
	String lastProgramHeader;
	String stopDate;
	String stopTime;
	
	//for night chart
	String targetDate;
	String nightStart;
	String nightEnd;
	String genStart;
	String genEnd;
	
	String stDate;
	String edDate;
	String isNight;
	String slideSeq;
	
}
