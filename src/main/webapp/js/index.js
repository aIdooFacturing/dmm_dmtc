
var className = "";
var classFlag = true;
var shopId=2;
function getNightlyMachineStatus(){
	var url = ctxPath + "/getNightlyMachineStatus.do";

	var param = "stDate=" + $("#sDate").val() + 
				"&shopId=" + shopId;

	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			var json = $.parseJSON(data);
			
			console.log(json)
			
			var tr = "";
					
			csvOutput = "No, 설비명, 날짜, 정지시간, 상태, 기준 날짜LINE";
			var status = "";
			var backgroundColor = "";
			var fontColor = "";
			
			$(".content").remove();
			$(json).each(function(idx, data){
				
				console.log(data)
				
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				
				console.log(data.status)
				if(data.status=="WAIT" || data.status=="NO-CONNECTION"){
					status = $compl;
					fontColor = "black";
					backgroundColor = "yellow";
				}else if(data.status=='ALARM'){
					status = $stopped;
					fontColor = "white";
					backgroundColor = "red";
				}else if(data.status=='IN-CYCLE'){
					status = $incycle;
					fontColor = "white";
					backgroundColor = "green";
				};
				
				if(data.isOld=="OLD"){
					status = $not_operating;
					fontColor = "white";
					backgroundColor = "black";
				};
				
				tr += "<tr style='font-Size:" + getElSize(35) + "px;' class='content '" + className + "' ondblClick=goSingleChart("+'"'+data.name+'"'+","+data.dvcId+")>"  +
					"<td  >" + (idx+1)  + "</td>" +
					"<td  > " + data.name + "</td>" +
					"<td >" + data.stopDate + "</td>" +
					"<td >" + data.stopTime + "</td>" + 
					"<td style='color:" + fontColor + "; background-color:" + backgroundColor + "; padding : " + getElSize(15) + "px;'>" + status + "</td>" +
					"<td >" + data.inputDate + "</td>" +
				"</tr>";
				
				csvOutput += (idx+1) + "," +
					data.name + "," + 
					data.stopDate + "," + 
					data.stopTime + "," + 
					status +  "," +
					data.inputDate + "LINE";
		
			});
			

			if(json.length==0){
				tr += "<tr  style='border:1px solid white; font-Size:" + getElSize(30) + "px;'>" + 
						"<td colspan='6'>없음</tb>" + 
					"<tr>";
			};
			
			$(".tmpTable").append(tr);
			
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			$("#wrapper div:last").remove();
			scrolify($('.tmpTable'), getElSize(1600));
			$("#wrapper div:last").css("overflow", "auto")
			//setEl();			
		}
	});
}

function goSingleChart(name, dvcId){
	console.log(name, dvcId)
	if(dvcId!=0){
		window.sessionStorage.setItem("dvcId", dvcId);
		window.sessionStorage.setItem("name", name);
		window.sessionStorage.setItem("date", $("#sDate").val());
		
		window.localStorage.setItem("dvcId",dvcId);
		location.href="/Single_Chart_Status/index.do";
	};
}
